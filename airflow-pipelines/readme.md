#### Generate FERNET KEY 
- Run only one time to generate unique fernet key that will be used to encpyt/decrypt credentials entered in Admin UI
> docker run puckel/docker-airflow python -c "from cryptography.fernet import Fernet; FERNET_KEY = Fernet.generate_key().decode(); print(FERNET_KEY)"
- Copy Key into:
  - `docker-compose.yml` -> webserver -> environment -> `AIRFLOW__CORE__FERNET_KEY`   
    
#### Run Airflow
> docker-compose up --build
    
- localhost:8080

#### Add connection credentials (i.e. AWS, DB) and input variables
- Admin -> Connection -> `aws_default `
  - Login = AWS_ACCESS_KEY
  - Password = AWS_SECRET_KEY 
  - Extra = {"region_name": "us-west-2"}

- Admin -> Variable
  - CSV_INPUT_S3_PATH = s3://curemetrix-elasticmapreduce/csv/input.csv