import boto3
import os
import pydicom
import json
from pyspark import SparkContext as sc
from pyspark.sql import SparkSession
import logging
import sys
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
        logging.StreamHandler()
    ]
)

def parse_dicom_from_df(row):
    bucket = row.bucket
    key = row.key
    target = row.target

    s3 = boto3.resource('s3')
    object1 = s3.Object(bucket, key)
    fileobj = object1.get()['Body'].read()

    fname = os.path.split(key)[1].replace('.dcm', '')
    accn = os.path.split(os.path.split(key)[0])[1]
    pid = os.path.split(os.path.split(os.path.split(key)[0])[0])[1]

    OUTPUT_BUCKET = 'curemetrix-elasticmapreduce'
    OUTPUT_KEY = target + '/' + pid + '/' + accn + '/' + fname

    # json header metadata
    dicom_bytes = pydicom.filebase.DicomBytesIO(fileobj)
    dicom_data = pydicom.filereader.dcmread(dicom_bytes)
    pixel_array = dicom_data.pixel_array
    del dicom_data[0x7FE0, 0x10]
    dict1 = dicom_data.to_json_dict()

    object3 = s3.Object(OUTPUT_BUCKET, OUTPUT_KEY + '.json')
    object3.put(Body=bytes(json.dumps(dict1).encode('UTF-8')))

    return (row.bucket, row.key, row.target, row.active)


if __name__ == "__main__":
    try:
        logger = logging.getLogger('pyspark')
        logger.info("My test info statement")

        csv_path = sys.argv[1]
        logger.info(csv_path)
        spark = SparkSession.builder.appName("Parse Dicom App").getOrCreate()
        df = spark.read.format('csv').options(header='true', inferSchema='true').load(csv_path)
        rdd = df.rdd.map(parse_dicom_from_df)
        rdd.count()
    except Exception as e:
        logger.error(e)