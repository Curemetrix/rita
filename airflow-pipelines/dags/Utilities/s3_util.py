import airflow.hooks.S3_hook

def upload_file_to_S3(filename, key, bucket_name):
    hook = airflow.hooks.S3_hook.S3Hook('aws_default')
    hook.delete_objects(bucket_name, key)
    hook.load_file(filename, key, bucket_name)
