import datetime
import logging
import json
import os

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator
from airflow.contrib.operators.emr_create_job_flow_operator import EmrCreateJobFlowOperator
from airflow.contrib.operators.emr_terminate_job_flow_operator import EmrTerminateJobFlowOperator
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.models import Variable
from Utilities.s3_util import upload_file_to_S3


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
        logging.StreamHandler()
    ]
)

EMR_BUCKET =  'curemetrix-elasticmapreduce'
EMR_BOOTSTRAP_KEY_PREFIX = 'bootstrap'
SPARK_JOB = 'spark_store_dicom_job.py'

with open('./data/emr/emr_settings.json') as json_file:
    default_emr_settings = json.load(json_file)
default_emr_settings['BootstrapActions'][0]['ScriptBootstrapAction']['Path'] = os.path.join("s3://", EMR_BUCKET, EMR_BOOTSTRAP_KEY_PREFIX + '/emr_bootstrap.sh')

csv_path = Variable.get("CSV_INPUT_S3_PATH")
run_step = [
        {
            "Name": SPARK_JOB,
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": ["spark-submit", "--deploy-mode", "cluster", "--master", "yarn",
                                        os.path.join("s3://", EMR_BUCKET, EMR_BOOTSTRAP_KEY_PREFIX + '/' + SPARK_JOB),
                                        csv_path]
            }
        }
    ]

default_args = {
    'owner': 'Airflow',
    'start_date': datetime.datetime.now() - datetime.timedelta(days=1),
    'retries': 1,
    'retry_delay': datetime.timedelta(seconds=5)
}

with DAG(
        'store-dicom-spark_process',
        schedule_interval=None,
        default_args=default_args,
        max_active_runs=1) as dag:

    start_operator = DummyOperator(task_id='Begin_execution')

    upload_sh_s3_task = PythonOperator(
        task_id="upload_emr_bootstrap_script_to_s3",
        python_callable=upload_file_to_S3,
        op_kwargs={
            'filename': './data/emr/emr_bootstrap.sh',
            'key': EMR_BOOTSTRAP_KEY_PREFIX + '/emr_bootstrap.sh',
            'bucket_name': EMR_BUCKET,
        }
    )
    upload_py_s3_task = PythonOperator(
        task_id="upload_scripts_for_spark_submit_to_s3",
        python_callable=upload_file_to_S3,
        op_kwargs={
            'filename': './data/emr/'+SPARK_JOB,
            'key': EMR_BOOTSTRAP_KEY_PREFIX + '/'+SPARK_JOB,
            'bucket_name': EMR_BUCKET,
        }
    )
    create_emr_job_flow_task = EmrCreateJobFlowOperator(
        task_id='create_emr_cluster',
        aws_conn_id='aws_default',
        emr_conn_id='emr_default',
        job_flow_overrides=default_emr_settings
    )
    add_emr_step_task = EmrAddStepsOperator(
        task_id='add_step_to_emr',
        job_flow_id="{{ task_instance.xcom_pull('create_emr_cluster', key='return_value') }}",
        aws_conn_id='aws_default',
        steps=run_step
    )
    watch_prev_step_task = EmrStepSensor(
        task_id='wait_for_emr_step_completion',
        job_flow_id="{{ task_instance.xcom_pull('create_emr_cluster', key='return_value') }}",
        step_id="{{ task_instance.xcom_pull('add_step_to_emr', key='return_value')[0] }}",
        aws_conn_id='aws_default'
    )
    terminate_emr_job_flow_task = EmrTerminateJobFlowOperator(
        task_id='terminate_emr_cluster',
        job_flow_id="{{ task_instance.xcom_pull('create_emr_cluster', key='return_value') }}",
        aws_conn_id='aws_default',
        trigger_rule="all_done"
    )

    end_operator = DummyOperator(task_id='Stop_execution')


# Configure the task dependencies:

start_operator >> upload_py_s3_task
start_operator >> upload_sh_s3_task
upload_py_s3_task >> create_emr_job_flow_task
upload_sh_s3_task >> create_emr_job_flow_task
create_emr_job_flow_task >> add_emr_step_task >> watch_prev_step_task >> terminate_emr_job_flow_task >> end_operator