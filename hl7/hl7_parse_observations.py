import os
import hl7
import pandas as pd
import argparse
import time
import datetime

BIRAD_TEXT = ["birads", "bi-rads"]
SIGNED_TEXT = "Electronically Signed By:"


def parse_observations(directory, out_file_path):
    '''
    parse these fields from hl7 into csv:
     PID_3 (pid), OBR_3 (accession), OBX_5 (observation, concatenated), OBX_14 (observation time)

    :param directory: directory of hl7 files to parse
    :param out_file_path: output location for parsed data in csv file
    '''

    file_num = 0
    t_0 = time.time()
    for filename in sorted(os.listdir(directory)):
        t_1 = time.time()
        if filename.endswith(".hl7"):
            print(os.path.join(directory, filename))
            file_path = os.path.join(directory, filename)

            file = open(file_path, mode='r',  encoding='utf-8', errors='ignore', newline='\r')
            message = file.read()
            file.close()

            h = hl7.parse(message)

            pid = h['PID'][0][3][0]
            accession = h['OBR'][0][3][0]
            obs_time = datetime.datetime.strptime(h['OBX'][0][14][0], '%Y%m%d%H%M%S')
            obs_time_raw = h['OBX'][0][14][0]

            birads_list = []
            observation_list = []
            for obx in h['OBX']:
                observation = str(obx[5][0])
                if observation != '' and SIGNED_TEXT not in observation:
                    observation_list.append(observation)

                    if any(b in observation.lower() for b in BIRAD_TEXT):
                        birads_list.append(observation)
                    if obs_time_raw != obx[14][0]:
                        raise Exception("datetime doesnt't match {}, {}"
                                        .format(obs_time_raw, obx[14][0]))

            file_num += 1
        else:
            continue

        if file_num == 1:
            df = pd.DataFrame([pid, accession, obs_time, ' '.join(birads_list), ' '.join(observation_list)]).T
            df.columns = ['pid', 'accession', 'observations_datetime', 'birads', 'observations']

            df.to_csv(out_file_path)
        else:
            df2 = pd.DataFrame([pid, accession, obs_time, ' '.join(birads_list), ' '.join(observation_list)]).T
            df2.columns = ['pid', 'accession', 'observations_datetime', 'birads', 'observations']

            df2.to_csv(out_file_path, mode='a', header=False)
 
        t_2 = time.time()

        #if file_num > 5:
        #    break
    print("Total files {}: {:0.2f}s".format(file_num, (t_2-t_0)))


def main():
    current_dt = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    parser = argparse.ArgumentParser(description="Parse observation data from list of hl7 files into single csv")
    parser.add_argument("-i", "--indir", type=str, help="input dir for hl7 files", required=True)
    parser.add_argument("-o", "--outdir", type=str, help="output dir for csv")
    parser.add_argument("-f", "--csvfile", type=str, help="filename for csv files",
                        default="hl7_observations_"+current_dt+".csv")
    args = parser.parse_args()

    out_path = args.outdir
    if out_path is None:
        out_path = args.indir

    out_file_path = os.path.join(out_path, args.csvfile)
    parse_observations(args.indir, out_file_path)


if __name__ == "__main__":
    main()
