import time
import datetime
import argparse
import os
import pandas as pd
from hl7apy import parser, set_default_version
from hl7apy.core import Group, Segment


set_default_version('2.3')


def parse_all(directory, out_file_path):
    '''
    parse all fields from hl7 into csv

    :param directory: directory of hl7 files to parse
    :param out_file_path: output location for parsed data in csv file
    '''

    file_num = 0
    df = pd.DataFrame()
    t_0 = time.time()
    for filename in sorted(os.listdir(directory)):
        t_1 = time.time()
        if filename.endswith(".hl7"):
            print(os.path.join(directory, filename))
            file_path = os.path.join(directory, filename)

            file = open(file_path, mode='r', encoding='utf-8', errors='ignore', newline='\r')
            message = file.read()
            file.close()

            df_col, df_field, df_obx = parse_hl7apy(message)
            file_num += 1
        else:
            continue

        if file_num == 1:
            df_hl7 = pd.DataFrame(df_field).T
            df_hl7.columns = df_col
            df_hl7['key'] = 0
            df_obx['key'] = 0
            df = df_hl7.merge(df_obx, how='outer')
            df.drop(columns=['key'], inplace=True)

        else:
            df = concat_df(df, df_obx, df_col, df_field)

        t_2 = time.time()
        print("File {}: {:0.2f}s".format(file_num,  (t_2-t_1)))

        # files to run
        if file_num > 10:
            break

    print("Total files {}: {:0.2f}s".format(file_num, (t_2-t_0)))
    df.to_csv(out_file_path)


def parse_hl7apy(message):
    '''
    parse hl7 message into distinct columns
    :param message: text from single hl7 message
    :return:
        df_columns: fields in MSH/PID/ORC/OBR segments parsed into list of column names
        df_values: fields in MSH/PID/ORC/OBR segments parsed into list of values for those column names
        df_obx: fields in multi-line OBX segments parsed into dataframe with column names
    '''

    msg = parser.parse_message(message.replace('\n', '\r'), find_groups=True, validation_level=2)

    df_columns = []
    df_values = []
    # Message
    for segment in msg.children:
        # MSH
        if isinstance(segment, Segment):
            for attribute in segment.children:
                df_columns.append(str(attribute.name) + " _" + str(attribute.long_name))
                df_values.append(attribute.value)

        # ORU segment containing PID/PV1/ORC/OBR/OBX segments
        if isinstance(segment, Group):
            last_field = ''
            df_obx = pd.DataFrame()
            for group in segment.children:
                for group_segment in group.children:
                    if isinstance(group_segment, Group):
                       # PV1
                        if str(group_segment.name) != 'ORU_R01_OBSERVATION':
                            for group_segment in group.children:
                                df_columns, df_values, last_field = parse_segment_into_fields(group_segment, df_columns,
                                                                                       df_values, last_field)

                        # OBX group
                        else:
                            # OBX is multi-line segments;
                            # return column names and values as a dataframe
                            df_obx = parse_obx(df_obx, group_segment)

                    elif group_segment.name != 'PID':
                        # PID, ORC, OBR
                        df_columns, df_values, last_field = parse_segment_into_fields(group_segment, df_columns, df_values,
                                                                               last_field)

    return df_columns, df_values, df_obx


def parse_obx(df_obx, obx_group):
    '''
    since OBX segment is multi-line, after parsing each segment into columns/values list,
    it will be concatednated into a dataframe
    '''

    # OBX group
    for group_segment in obx_group.children:
        #if isinstance(group_segment, Group):
        #    parse_obx(group_segment)

        df_columns = []
        df_values = []
        last_field = ''
        df_columns, df_values, last_field = parse_segment_into_fields(group_segment, df_columns, df_values, last_field)

    if df_obx.size == 0:
        df_obx = pd.DataFrame(df_values).T
        df_obx.columns = df_columns
    else:
        df_obx = concat_df(df_obx, None, df_columns, df_values)

    return df_obx


def parse_segment_into_fields(group_segment, df_columns, df_values, last_field):
    '''
    parse each field value into single column, ignoring repeated columns and repetition symbol '~'

    :param group_segment: MSH/PID/PV1/ORC/OBR/OBX
    :param df_columns: list of column names for segment
    :param df_values: list of field values for segment
    :param last_field: used to track last column field (repeated fields will have their values concat)
    :return:
    '''

    for field in group_segment.children:
        if field.name is None:
            continue
        if field.name != last_field:
            df_columns.append(str(field.name) + " _" + str(field.long_name))
            df_values.append(field.value)
        else:
            # concat fields with REPETITION ~
            df_values[-1] = df_values[-1] + '~' + field.value
        last_field = field.name

    return df_columns, df_values, last_field


def concat_df(df_src, df_add, df_columns, df_values):
    '''
    concatenate dataframes handling mis-matched columns in the 2 dataframes

    :param df_src: source dataframe being concatenated to
    :param df_add: additional dataframe to join before concat with df_src
    :param df_columns: additional columns to join to df_new
    :param df_values: field values for additional columns to join to df_new
    :return:
    '''

    # convert list of columns and values into new dataframe to concat
    df_new = pd.DataFrame(df_values).T
    df_new.columns = df_columns

    if df_add is not None:
        # add multi-line OBX fields to join with single line MSH/PID/PV1/ORC/OBR fields in df
        df_new['key'] = 0
        df_add['key'] = 0
        df_new = df_new.merge(df_add, how='outer')
        df_new.drop(columns=['key'], inplace=True)

    # add new column to source df
    new_col = (list(set(df_new.columns) - set(df_src.columns)))
    new_col.sort(key=lambda x: x.split('_')[1])
    if len(new_col) > 0:
        for c in new_col:
            if c not in df_src.columns:
                loc = df_new.columns.get_loc(c)
                df_src.insert(loc, c, '')
    # add column from source missing in df to concat
    missing_col = list(set(df_src.columns) - set(df_new.columns))
    if len(missing_col) > 0:
        missing_col.sort(key=lambda x: x.split('_')[1])
        for c in missing_col:
            if c not in df_new.columns:
                loc = df_src.columns.get_loc(c)
                df_new.insert(loc, c, '')

    df = pd.concat([df_src, df_new], ignore_index=True)
    return df


def main():
    current_dt = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    parser = argparse.ArgumentParser(description="Parse all columns from list of hl7 files into single csv")
    parser.add_argument("-i", "--indir", type=str, help="input dir for hl7 files", required=True)
    parser.add_argument("-o", "--outdir", type=str, help="output dir for csv")
    parser.add_argument("-f", "--csvfile", type=str, help="filename for csv files",
                        default="hl7_full_"+current_dt+".csv")
    args = parser.parse_args()

    out_path = args.outdir
    if out_path is None:
        out_path = args.indir

    out_file_path = os.path.join(out_path, args.csvfile)
    parse_all(args.indir, out_file_path)


if __name__ == "__main__":
    main()
