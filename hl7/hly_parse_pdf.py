import os
import hl7
import pandas as pd
import argparse
import time
import datetime
import codecs


def parse_observations(directory, out_path):
    file_num = 0
    for filename in sorted(os.listdir(directory)):
        if filename.endswith(".hl7"):
            print(os.path.join(directory, filename))
            file_path = os.path.join(directory, filename)
            # open hl7
            file = open(file_path, mode='r', encoding='utf-8', errors='ignore', newline='\r')
            message = file.read()
            file.close()
            # parse hl7 message
            h = hl7.parse(message)
            pid = h['PID'][0][3][0]
            accession = h['OBR'][0][3][0]
            pdf = h['OBX'][0][5][0]
            str_to_bytes = pdf.encode('utf-8')
            binaryfile = codecs.decode(str_to_bytes, 'base64')
            fname = accession+'.pdf'
            datafile=open(os.path.join(out_path, fname),'wb')
            datafile.write(binaryfile)
            datafile.close()

        file_num += 1
        #if file_num > 5:
        #    break

def main():
    parser = argparse.ArgumentParser(description="Parse observation data from list of hl7 files into single csv")
    parser.add_argument("-i", "--indir", type=str, help="input dir for hl7 files", required=True)
    parser.add_argument("-o", "--outdir", type=str, help="output dir for pdf")
    args = parser.parse_args()
    out_path = args.outdir
    if out_path is None:
        out_path = args.indir
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    parse_observations(args.indir, out_path)


if __name__ == "__main__":
    main()