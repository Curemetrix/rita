import argparse
import os
import io
import pandas as pd
import cv2
import numpy as np
from pdf2image import convert_from_path
from pytesseract import pytesseract as pt
from PIL import Image


def image_processing(imgByteArr):
    img = np.frombuffer(imgByteArr, dtype='uint8')
    image = cv2.imdecode(img, cv2.IMREAD_COLOR)

    img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (2, 2))
    image = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

    return image

def perform_ocr(input, output):
    file_num = 0
    for filename in sorted(os.listdir(input)):
        if filename.endswith("pdf"):
            pages = convert_from_path(os.path.join(input, filename), dpi=300)
            #convert_from_bytes
            print(filename)
            page_num = 0
            outpath_txt = os.path.join(output, 'txt', filename.replace('.pdf', '.txt'))
            outpath_csv = os.path.join(output, 'csv', filename.replace('.pdf', '.csv'))
            text_file = open(outpath_txt, "a")
            df = pd.DataFrame()
            for page in pages:
                page_num += 1
               # outpath_pdf = os.path.join(output, 'pdf', filename.replace('.pdf', '_'+str(page_num)+'.pdf'))
               # pdf_file = open(outpath_pdf, "ab")
               # img_fullpath = os.path.join(output, 'png', filename.replace('.pdf', '_'+str(page_num)+'.png'))
               # page.save(img_fullpath, 'PNG')

                imgByteArr = io.BytesIO()
                page.save(imgByteArr, format='PNG')
                imgByteArr = imgByteArr.getvalue()

                image = Image.open(io.BytesIO(imgByteArr)) #


                custom_oem_psm_config = '--oem 3 --psm 6 -c textord_heavy_nr=0 ' \
                                        'tessedit_parallelize=1 ' \
                                    #    'tessedit_char_whitelist=1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz '
                text = pt.image_to_string(image, lang='eng', config=custom_oem_psm_config)
                text_file.write(text)

                #pdf = pt.image_to_pdf_or_hocr(image, extension='pdf')
                #pdf_file.write(pdf)
                #pdf_file.close()
                df_tmp = pt.image_to_data(image, lang='eng', output_type=pt.Output.DATAFRAME,
                                            config=custom_oem_psm_config)
                if page_num == 1:
                    df = df_tmp
                else:
                    df.append(df_tmp)
               # if page_num > 0:
               #     break
            text_file.close()

            df.to_csv(outpath_csv, index=False)
            file_num += 1
            join_text = ' '.join(df.dropna().text.tolist())
            new_df = pd.DataFrame([[filename.replace('.pdf', ''), join_text]], columns=['accession', 'text'])
            if file_num == 1:
                new_df.to_csv(os.path.join(output, 'combined.csv'), index=False)
            else:
                new_df.to_csv(os.path.join(output, 'combined.csv'), mode='a', header=False, index=False)

        #if file_num > 0:
        #    break


def main():
    parser = argparse.ArgumentParser(description="Parse data from list of hl7 files into single csv")
    parser.add_argument("-i", "--indir", type=str, help="input dir for pdf scan files", required=True)
    parser.add_argument("-o", "--outdir", type=str, help="output dir for csv")
    args = parser.parse_args()
    out_path = args.outdir
    if out_path is None:
        out_path = args.indir
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    perform_ocr(args.indir, out_path)


if __name__ == "__main__":
    main()