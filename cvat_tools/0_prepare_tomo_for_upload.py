import argparse
import shutil
import os
import time
from Utilities.concat_image import concat_img_to_reference, concat_movie_to_reference, copy_resize, get_tomo_image_height
from Utilities.dataframe_util import DataframeUtil
import multiprocessing
from functools import partial
import logging

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
       # logging.FileHandler("debug.log"),
        logging.StreamHandler()
    ]
)
NUM_PROCESSES = multiprocessing.cpu_count()

def send_to_concat_parallel(movie_path, img_path, df, outdir):
    df_tomo = df.get_tomo(True)

    chunk_size = int(df_tomo.shape[0] / NUM_PROCESSES)
    print(df_tomo.shape[0], chunk_size)
    if chunk_size == 0:
        chunk_size = 1
    chunks = [df_tomo.iloc[i:i + chunk_size, :] for i in range(0, df_tomo.shape[0], chunk_size)]

    pool = multiprocessing.Pool(processes=NUM_PROCESSES)
    func = partial(concat_image, movie_path, img_path, df, outdir)
    pool.map(func, chunks)
    pool.close()
    pool.join()


def concat_image(movie_path, img_path, df, outdir, df_chunk):
    for i, row in df_chunk.iterrows():

        sop = row['sopuid']
        outdir_sop = os.path.join(outdir, sop)
        logging.info("Working on # {}: {}".format(i, sop))

        if os.path.exists(os.path.join(movie_path, sop)):
            moviepath = os.path.join(movie_path, sop)
        else:
            logging.error("Movie path does not exists for " + sop)
            raise
            #continue

        if os.path.exists(moviepath):
            if len(os.listdir(moviepath)) == 0:
                logging.error("Movie path does not contain items for " + sop)
                raise
                #continue
            if os.path.exists(outdir_sop) and len(os.listdir(outdir_sop)) == 0:
                os.rmdir(outdir_sop)
            #    continue
            if not os.path.exists(outdir_sop):
                os.makedirs(outdir_sop)
                logging.info("Processing # {}: {}".format(i, sop))
            else:
                logging.info("Skipping # {}: {}".format(i, sop))
                continue

            # find 2D reference view for tomo and join image to tomo frames
            # if no 2D view exists, use first GT slice
            flip_ref = False
            ref_sop = df.get_reference_sop(row)
            if len(ref_sop) > 0 and os.path.exists(os.path.join(img_path, ref_sop[0]+'.png')):
                ref_file = ref_sop[0]
                ref_path = os.path.join(img_path, ref_file + '.png')
                if row['image_laterality'].upper() == 'L':
                    flip_ref = True
            else:
                ref_frame = int(df.get_first_GT_frame(row['sopuid']))
                logging.info("Using GT Frame as reference: " + str(ref_frame))
                ref_file = sop + "-" + ('0'+str(ref_frame))[-2:]
                ref_path = os.path.join(moviepath, ref_file + ".png")

            if not os.path.exists(ref_path):
                logging.error("No reference image found at: " + ref_path)
                raise
                #continue

            t_0 = time.time()
            file_cnt = concat_movie_to_reference(moviepath, ref_path, outdir_sop, flip_ref, df)
            logging.info("Time to join # {} SOP: {} ({} frames): {:0.2f}s".format(i, sop, file_cnt, (time.time() - t_0)))

            h_orig = get_tomo_image_height(moviepath)

            # merge other views to reference frame
            other_path = os.path.join(outdir_sop, 'other')
            if not os.path.exists(other_path):
                os.makedirs(other_path)

            df_other = df.filt_by_pid_accn(row['patient_name'], row['accession_number'])
            for i_other, row_other in df_other.iterrows():
                other_file = os.path.join(img_path, row_other['sopuid'] + '.png')
                if os.path.exists(other_file):
                    if len(ref_sop) == 0 or row_other['sopuid'] != ref_sop[0]:
                        new_filename = "z_" + ref_file + '_' + row_other['sopuid'] + '.png'
                        out_fullpath = os.path.join(other_path, new_filename)

                        concat_img_to_reference(ref_path, other_file, out_fullpath, h_orig, False, df)


def send_to_concat(movie_path, img_path, df, outdir):
    df_tomo = df.get_tomo(True)
    concat_image(movie_path, img_path, df, outdir, df_tomo)


def main():
    parser = argparse.ArgumentParser(description="Concats each image frame in tomo movie with reference 2D view")
    parser.add_argument('-i', '--indir', type=str, help="input dir for tomo movies and pngs", required=True)
    parser.add_argument('-o', '--outdir', type=str, help="output dir for tomo movies concat with 2D", required=True)
    parser.add_argument('-f', '--file', type=str, help="csv file with list of sop and GT", required=True)
    args = parser.parse_args()

    df_util = DataframeUtil(args.indir, args.file)

    movie_path = os.path.join(args.indir, 'movie')
    img_path = os.path.join(args.indir, 'png2')

    t_0 = time.time()
    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)
    send_to_concat_parallel(movie_path, img_path, df_util, args.outdir)
    logging.info("Total time: {:0.2f}s".format(time.time() - t_0))


if __name__ == '__main__':
    main()
