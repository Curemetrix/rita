#!/bin/bash
FILES=/home/ec2-user/data/Tomo_with_2D/movie/*
for f in $FILES
do
  file1=$(basename $f)
  file=${file1%.tar.gz}
  echo "Processing file $file..."
  #mkdir /home/ec2-user/cvat_share/Tomo_GT_Check/movie/$file
  tar xvzf $f --strip-components 4 -C /home/ec2-user/cvat_share/Tomo_GT_Check/movie/

done