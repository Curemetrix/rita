import argparse
import os
import time
from CvatUtil.CvatApi import CvatApi
import logging
import random

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
       # logging.FileHandler("debug.log"),
        logging.StreamHandler()
    ]
)


def create_cvat_task(img_path):
    t0 = time.time()
    cnt = 0
    file_list = os.listdir(img_path)
    print(file_list)
    random.shuffle(file_list)
    print(file_list)
    for filename in file_list:
        if filename.endswith(".png"):
            print(filename)
            # create task, get task_id
            new_api = CvatApi(name_prefix="[Density Reader Study] " + filename.replace('.png', ''), label='Density')
            task_id = new_api.create_task()

            # create job in task
            frame_cnt = new_api.add_image_list_to_task(task_id, [os.path.join(img_path, filename)])
            logging.info("Uploading {}".format(filename))
            t_0 = time.time()
            job_created = False
            while not job_created:
                time.sleep(1)
                job_created, job_id = new_api.get_last_job_id(task_id, 1)
            t_1 = time.time()
            logging.info("Upload time for Task# {} Job# {}: {:0.2f}s"
                      .format(task_id, job_id, t_1 - t_0))

            # add annotation tag
            new_api.add_annotation_tag(task_id, job_id)

            t1 = time.time()
            logging.info("Total time for Task# {}: {:0.2f}s".format(task_id, job_id, t1 - t0))
            cnt += 1
            #if cnt > 0:
            #    break

def main():
    parser = argparse.ArgumentParser(description="Uploads images")
    parser.add_argument('-i', '--indir', type=str, help="input dir for pngs", required=True)
    args = parser.parse_args()

    if os.path.exists(args.indir):
        img_path = args.indir
        t_0 = time.time()
        create_cvat_task(img_path)
        logging.info("Total time: {:0.2f}s".format(time.time() - t_0))
    else:
        logging.error("Path does not exists:".format(args.indir))


if __name__ == '__main__':
    main()
