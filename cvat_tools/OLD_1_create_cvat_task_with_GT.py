import argparse
import numpy as np
import pandas as pd
import os
import time
import glob
from CvatUtil.CvatApi import CvatApi
import cv2
import logging

logging.getLogger().setLevel(logging.WARN)


def create_cvat_task(img_path, df_filt, extra_dir):
    t0 = time.time()
    px_notes = []
    accn = os.path.split(os.path.split(os.path.split(extra_dir)[0])[0])[1]
    pid = os.path.split(os.path.split(os.path.split(os.path.split(extra_dir)[0])[0])[0])[1]
    new_api = CvatApi()

    # create task, get task_id
    new_api.task.name = "[Tomo with 2D] PID: " + pid + " ACCN: " + accn
    task_id = new_api.create_task()

    # loop through sop
    job_cnt = 0
    sop_list = df_filt[df_filt['is_tomo'] == True]['sopuid'].unique()
    print(sop_list)
    for sopuid in sorted(sop_list):
        print("\n" + str(sopuid))
        img_path = os.path.join(img_path, sopuid)
        extra_path = os.path.join(extra_dir, sopuid)

        #if not os.path.isdir(img_path):
        #    continue

        df_sop = df_filt[df_filt['sopuid'] == sopuid]
        df_sop_filt = df_sop[['study_description', 'side']].drop_duplicates()

        more_info = df_sop_filt.apply(lambda x: x['study_description'] + " (" + x['side'] + ")", axis=1).to_list()[0]
        px_notes.append(more_info)

        # create job in task
        frame_cnt = new_api.add_data_to_task(task_id, img_path, extra_path)

        job_cnt += 1
        t_0 = time.time()
        job_created = False
        job_id = 290
        #while not job_created:
        #    time.sleep(1)
        #    job_created, job_id = new_api.get_last_job_id(task_id, job_cnt)

        t_1 = time.time()
        print("Upload time for Task#{} Job#{} Frame Count ({}): {:0.2f}s"
              .format(task_id, job_id, frame_cnt, t_1 - t_0))

        px_notes.append("Job #" + str(job_id) + " Tomo Frames: 0-" + str(frame_cnt-1))

        # add existing annotation for image frames
        df_bbox = df_sop.apply(lambda x: (x['frame_number'], [x['xmin'], x['ymin'], x['xmax'], x['ymax']]), axis=1)
        bbox = df_bbox.to_list()
        for b in bbox:
            px_notes.append("Tomo GT Frame " + str(b[0]) + " bbox " + str(b[1]))


        path_2d = os.path.join(os.path.split(extra_dir)[0], '2D', sopuid)
        img_2d = [os.path.split(f)[1].replace('.png', '') for f in glob.glob(os.path.join(path_2d, '*.png'))]
        offset = cv2.imread(os.path.join(path_2d, img_2d[0]+'.png')).shape[1]
        for sop_2d in sorted(img_2d):
            df_2d = df_filt[df_filt['sopuid'] == sop_2d]
            box_list = df_2d.apply(lambda x: [x['xmin'], x['ymin'], x['xmax'], x['ymax']], axis=1).to_list()
            box_list_offset = df_2d.apply(lambda x: [x['xmin'] + offset, x['ymin'], x['xmax'] + offset, x['ymax']],
                               axis=1).to_list()

        for b in box_list_offset:
            for i in range(0, frame_cnt, 1):
                bbox.append((i, b))
        for b in box_list:
            px_notes.append("2D GT (SOP: "+ img_2d[0] +") bbox " + str(b))

        # loop through extra_dir
        extra_img = [os.path.split(f)[1].replace('.png', '') for f in glob.glob(os.path.join(extra_path, '*.png'))]
        frame = frame_cnt
        for extra_sop in sorted(extra_img):
            df_other = df_filt[df_filt['sopuid'] == extra_sop]
            df_bbox = df_other.apply(lambda x: [x['xmin'], x['ymin'], x['xmax'], x['ymax']], axis=1).to_list()
            px_notes.append("Other GT Frame " + str(frame) + "(SOP: "+ extra_sop +") bbox " + str(df_bbox))
            bbox.append((frame, df_bbox))
            frame += 1

        if len(bbox) > 0:
            new_api.add_annotations(task_id, job_id, bbox)

        t1 = time.time()
        print("Total time for Task#{}: {:0.2f}s".format(task_id, job_id, t1 - t0))

    px_notes_str = ' ////// '.join(p for p in px_notes)

    # update task_id with notes for "bug_tracker" field
    new_api.update_task_notes(task_id, px_notes_str)


def main():
    parser = argparse.ArgumentParser(description="Uploads tomo movie + 2D view into CVAT")
    parser.add_argument('--basedir', type=str, help="base dir for everything", required=True,
                        default='/Users/rita/dev/Tomo_with_2D/')
    parser.add_argument('--extra_dir', type=str, help="sub dir for other views",
                        default='OtherView')
    parser.add_argument('--img_dir', type=str, help="sub dir where tomo concat images",
                        default="ref_concat")
    parser.add_argument('--csv', type=str, help="csv with list of sop and GT", default='demoCase.csv')
    args = parser.parse_args()

    df = pd.read_csv(os.path.join(args.basedir, args.csv), encoding="utf-8")
    #df_dir = df[['patient_name', 'accession_number']].drop_duplicates()
    #subdir_list = df_dir.apply(lambda x: (x['patient_name'].replace('^', ''), str(x['accession_number'])),
    #                          axis=1).to_list()

    #for sub in subdir_list:
    #    df_filt = df[np.logical_and(df['patient_name'].apply(lambda x: x.replace('^','')) == sub[0],
    #                                df['accession_number'] != sub[1])]
    img_path = os.path.join(args.basedir, args.img_dir)
    extra_dir = os.path.join(args.basedir, args.extra_dir)
    create_cvat_task(img_path, df, extra_dir)


if __name__ == '__main__':
    main()
