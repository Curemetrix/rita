import json
import os
import requests
from urllib.parse import quote
from Utilities.populate_model import populate_default_task
import logging

SERVER = "http://ec2-34-218-87-47.us-west-2.compute.amazonaws.com/"
SERVER = "http://localhost"
AUTH = 'Basic '
SERVER = "http://ec2-35-161-29-166.us-west-2.compute.amazonaws.com/" # external
HEADER_JSON = {
    'Authorization': AUTH,
    'Content-Type': "application/json"
}


class CvatApi(object):

    def __init__(self, name_prefix="", pid=None, accn=None, sop=None, label=None):
        self.task = populate_default_task(name_prefix, pid, accn, sop, label)

    def set_task_name(self, name_prefix="", pid=None, accn=None, sop=None):
        self.task.name = "[{}] PID {} ACCN {} SOP {}".format(name_prefix, pid, accn, sop)

    def create_task(self):
        route = "/api/v1/tasks"
        payload = json.dumps(self.task.__dict__, default=lambda o: o.__dict__, indent=4)

        try:
            response = requests.request("POST", SERVER+route, headers=HEADER_JSON, data=payload)
            logging.info(response.text)
        except Exception as e:
            logging.error(e)

        task_created = response.json()
        task_id = task_created['id']
        return task_id

    def update_task_notes(self, task_id, notes):
        route = f"/api/v1/tasks/{task_id}"
        data = json.dumps({"bug_tracker": notes})
        try:
            pass
            response = requests.request("PATCH", SERVER+route, headers=HEADER_JSON, data=data)
            logging.info(response.text)
        except Exception as e:
            logging.error(e)

    def delete_task_and_data(self, task_id):
        route = f"/api/v1/tasks/{task_id}"
        try:
            response = requests.request("DELETE", SERVER+route, headers=HEADER_JSON)
            logging.info(response.text)
        except Exception as e:
            logging.error(e)

        return response

    def does_task_exists(self, search_text):
        route = f"/api/v1/tasks?search="
        text_encoded = quote(search_text)
        try:
            response = requests.request("GET", SERVER+route+text_encoded, headers=HEADER_JSON)
            logging.info(response.text)
        except Exception as e:
            logging.error(e)

        task = response.json()
        print(task)
        task_exists = bool(task['count'])

        return task_exists

    def get_last_job_id(self, task_id, job_cnt):
        route = f"/api/v1/tasks/{task_id}"
        try:
            response = requests.request("GET", SERVER+route, headers=HEADER_JSON)
           # logging.info(response.text)
        except Exception as e:
            logging.error(e)

        task = response.json()
        segment_cnt = len(task['segments'])

        job_id = None
        while segment_cnt < job_cnt:
            return False, 0
        if segment_cnt == job_cnt:
            job_id = task['segments'][job_cnt-1]['jobs'][0]['id']
        return True, job_id

    def add_image_list_to_task(self, task_id, files_list):
        route = f"/api/v1/tasks/{task_id}/data"
        headers = {
            'Authorization': AUTH
        }

        try:
            files, stop_frame = self._get_data_with_files_list(files_list)

            data = {"image_quality": 95,
                    "chunk_size": 7,
                    "use_zip_chunks": True
                    }
            print(files)
            response = requests.request("POST", SERVER+route, headers=headers, data=data, files=files)
           # logging.info(response.text)
        except Exception as e:
            logging.error(e)
            return 0

        return stop_frame+1

    def add_data_to_task(self, task_id, file_dir, extra_dir=None, is_server=False):
        route = f"/api/v1/tasks/{task_id}/data"
        headers = {
            'Authorization': AUTH
        }

        try:
            files, stop_frame = self._get_data_with_files(file_dir, extra_dir, is_server)

            data = {"image_quality": 95,
                    "chunk_size": 7,
                    "use_zip_chunks": True
                    }
            response = requests.request("POST", SERVER+route, headers=headers, data=data, files=files)
            #logging.info(response.text)
        except Exception as e:
            logging.error(e)
            return 0

        return stop_frame+1

    def _get_data_with_files_list(self, files_list):
        file_index = 0
        files = {}
        for file_path in files_list:
            file_path
            if file_path.endswith(".jpg") or file_path.endswith(".png"):
                local_file = (os.path.split(file_path)[1], open(file_path, "rb"))
                file_key = "client_files[{}]".format(file_index)
                files[file_key] = local_file
                file_index += 1

        stop_frame = file_index - 1
        return files, stop_frame

    def _get_data_with_files(self, img_path, extra_dir=None, is_server=False):
        file_index = 0
        files = {}
        file_index, files = self._add_to_files(img_path, file_index, files, is_server)
        stop_frame = file_index - 1
        if extra_dir is not None and os.path.exists(extra_dir):
            file_index, files = self._add_to_files(extra_dir, file_index, files, is_server)

        return files, stop_frame

    def _add_to_files(self, path, file_index, files, is_server):
        for filename in sorted(os.listdir(path)):
            if filename.endswith(".jpg") or filename.endswith(".png"):
                file_path = os.path.join(path, filename)
                if is_server:
                    file_key = "server_files[{}]".format(file_index)
                    files[file_key] = file_path
                else:
                    local_file = (filename, open(file_path, "rb"))
                    file_key = "client_files[{}]".format(file_index)
                    files[file_key] = local_file
                file_index += 1
        return file_index, files

    def add_annotations(self, task_id, job_id, bbox):
        route = f"/api/v1/jobs/{job_id}/annotations"
        label_id = self._get_task_label_id(task_id)
        shapes = []
        for box in bbox:
            shape = {
                        "type": "rectangle",
                        "occluded": False,
                        "z_order": 0,
                        "points": box[1],
                        "frame": box[0],
                        "label_id": label_id,
                        "group": 0,
                        "attributes": []
                    }
            shapes.append(shape)

        data = {
            "tags": [],
            "version": 0,
            "shapes": shapes,
            "tracks": []
        }

        data = json.dumps(data)
        # print(data)
        try:
            response = requests.request("PUT", SERVER+route, headers=HEADER_JSON, data=data)
            logging.info(response.text)
        except Exception as e:
            logging.error(e)

    def add_annotation_tag(self, task_id, job_id):
        route = f"/api/v1/jobs/{job_id}/annotations"
        label_id = self._get_task_label_id(task_id)
        tags = []
        tag = {
                "frame": 0,
                "label_id": label_id,
                "group": 0,
                "attributes": []
            }
        tags.append(tag)

        data = {
            "tags": tags,
            "version": 0,
            "shapes": [],
            "tracks": []
        }

        data = json.dumps(data)
        # print(data)
        try:
            response = requests.request("PUT", SERVER+route, headers=HEADER_JSON, data=data)
            logging.info(response.text)
        except Exception as e:
            logging.error(e)

    def _get_task_label_id(self, task_id):
        route = f"/api/v1/tasks/{task_id}"
        try:
            response = requests.request("GET", SERVER+route, headers=HEADER_JSON)
            logging.info(response.text)
        except Exception as e:
            logging.error(e)

        task = response.json()
        label_id = task['labels'][0]['id']
        return label_id