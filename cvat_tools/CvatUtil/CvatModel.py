
class Task:
    def __init__(self):
        self.name = ""
        self.owner = 0
        self.assignee = 0
        self.bug_tracker = ""
        self.overlap = 0
        self.segment_size = 0
        self.z_order = "true"
        self.labels = []
        #self.project = 0


class Label:
    def __init__(self):
        self.name = ""
        self.color = "#fa3253"
        self.attributes = []


class Attributes:
    def __init__(self):
        self.name = ""
        self.mutable = True
        self.input_type = AttributeInputType.INPUT_TYPE_TEXT
        self.default_value = ""
        self.values = []


class AttributeInputType:
    INPUT_TYPE_TEXT = "text"
    INPUT_TYPE_SELECT = "select"
    INPUT_TYPE_RADIO = "radio"
    INPUT_TYPE_CHECKBOX = "checkbox"
    INPUT_TYPE_NUMBER = "number"
