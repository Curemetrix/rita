import glob
import time
import os
import argparse
import numpy as np
import pandas as pd
from CvatUtil.CvatApi import CvatApi
import logging

logging.getLogger().setLevel(logging.WARN)


def delete_task(task_id=None, start=None, stop=None):
    api = CvatApi()

    if task_id is not None:
        res = api.delete_task_and_data(task_id);
        print(res.text)
    elif start is not None and stop is not None:
        for id in range(start, stop, 1):
            print("Deleting task " + str(id))
            res = api.delete_task_and_data(id);
            print(res.text)


if __name__ == '__main__':
    delete_task(task_id=106) #start=130, stop=135)
