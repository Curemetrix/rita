
## CVAT installation

### Setup CVAT on ec2 (with S3 and RDS to store data)
 - Clone repo: `git clone curemetrix-cvat/cm-develop`
 - Modify `docker-compose.override.yml` with credentials to RDS 
    - `cvat-database.c0ncckddkgdv.us-west-2.rds.amazonaws.com`
 - Modify `docker-compose.yml` with credentials to S3 bucket 
    - `/cvat-data-storage/internal`
    
 - `docker-compose build`
 - `docker-compose up -d`
 - `docker exec -it cvat bash -ic 'python3 ~/manage.py migrate'`
 - `docker exec -it cvat bash -ic 'python3 ~/manage.py createsuperuser'`

## CVAT data upload

### Create s3 mount on ec2 instance 
https://curemetrix.atlassian.net/wiki/spaces/IT/pages/12746758/CureMetrix+FTP+Server+on+EC2
https://cloud.netapp.com/blog/amazon-s3-as-a-file-system
 - `sudo apt-get install s3fs`
 - `echo ACCESS_KEY:SECRET_KEY > .passwd-s3fs`
 - `chmod 600 .passwd-s3fs`
 - `mkdir ${HOME}/cvat_share`
 - s3fs cvat-share /home/ubuntu/cvat_share -opasswd_file=.passwd-s3fs -oendpoint=us-west-2 -oumask=0000 -ouse_cache=/tmp/cache

umount -l /home/ubuntu/cvat_share
> sudo chmod 755 -R ${HOME}/cvat_share
> sudo chmod -R o+rw  s3_cvat-data/

### Clone repo for cvat_tools
 - Clone repo: `git clone git@bitbucket.org:Curemetrix/rita.git`
 - Set up venv for cvat_tools
    - `sudo apt-get update`
    - `sudo apt-get upgrade`
    - `sudo apt-get install python3-venv`
    - `python3 -m venv my-env`
    - `source my-env/bin/activate`

- How to resolve `ImportError: No module named skbuild`
 apt install python-pip
 pip install --upgrade pip
 pip install opencv-python==4.2.0.32
 
 `ImportError: libSM.so.1: cannot open shared object file: No such file or directory`
sudo apt update
sudo apt install libgl1-mesa-glx
sudo apt install libsm6 libxext6 libxrender-dev
### Zip and copy files to s3-cvat_share
 - For tomo, copy movie and 2d dicom/json to directory
 - `tar -czvf filedir.tar.gz /files/path`
-filter csv for pid/acn sops
extract movie
extract_targz_files_in_dir.sh

root/movie
python ~proc/study_pull_results.py --pull-dcm --study 1.0.18.743 tomo-study.csv $DIR_TO_DOWNLOAD
docker pull 710761364787.dkr.ecr.us-west-2.amazonaws.com/curemetrix/processing-utils:latest
docker tag 710761364787.dkr.ecr.us-west-2.amazonaws.com/curemetrix/processing-utils:latest processing

- render images for dicom to png
loop_render_image.sh
root/png

df[df['sopuid'].isin(sop)].apply(lambda x: x['patient_name'].replace('^','')+'/'+str(x.accession_number), axis=1).unique().tolist()

# merge tomo frame to 2D view
python ./0_prepare_tomo_for_upload.py -i /home/ubuntu/cvat_share/Tomo_GT_Check/ -o /home/ubuntu/cvat_share/Tomo_GT_Check/for_cvat_filt2 -f /home/ubuntu/cvat_share/Tomo_GT_Check/tomo_calc_gts_missing.csv 

output=> root/for_cvat

# upload to cvat
lsblk
df 
sudo growpart /dev/nvme0n1 1
sudo resize2fs /dev/nvme0n1p1
python ./1_upload_tomo_to_cvat.py -b /home/ubuntu/cvat_share/Tomo_GT_Check -i for_cvat_filt2 -f filt_calc_gts_pt3.csv 

python 0_upload_images_to_cvat.py -i /home/ubuntu/cvat_share/cmangio -f /home/ubuntu/cvat_share/cmangio/bac_annotations.csv
