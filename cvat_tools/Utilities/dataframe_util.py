import pandas as pd
import numpy as np
import os

class DataframeUtil(object):

    def __init__(self, basedir, csv):
        self.df = pd.read_csv(os.path.join(basedir, csv), encoding="utf-8")
        self.df_filt = self.df

    def get_pid_accn_list(self, tomo=False):
        if tomo:
            new_df = self.df[self.df['is_tomo'] == True][['patient_name', 'accession_number']]
        else:
            new_df = self.df

        df_dir = new_df[['patient_name', 'accession_number']].drop_duplicates()
        pidacn_list = df_dir.apply(lambda x: (x['patient_name'].replace('^', ''), str(x['accession_number'])), axis=1).to_list()
        return pidacn_list

    def get_tomo(self, isTomo=True):
        df_filt = self.df[self.df['is_tomo']==isTomo][['sopuid', 'patient_name', 'accession_number',
                                                       'series_description', 'image_laterality','view_position']].drop_duplicates()
        return df_filt

    def filt_by_pid_accn(self, pid, accn, isTomo=False):
        pid_accn_filt = np.logical_and(self.df['patient_name'] == pid, self.df['accession_number'] == accn)

        df_filt = self.df[np.logical_and(self.df['is_tomo'] == isTomo, pid_accn_filt)][['sopuid',
                                                              'patient_name', 'accession_number',
                                                             'series_description', 'image_laterality',
                                                             'view_position']].drop_duplicates()
        return df_filt

    def get_series_info(self, pid, accn, isTomo=True):
        df_sop_filt = self.filt_by_pid_accn(pid, accn, isTomo)
        series_info = df_sop_filt.apply(lambda x: x['series_description']
                                    + " (" + x['image_laterality'] + " " + x['view_position'] +")", axis=1).to_list()[0]
        return series_info

    def get_reference_sop(self, row):
        pid = row['patient_name']
        accn = row['accession_number']
        df_other = self.get_tomo(False)
        series_filt = df_other['series_description'].apply(lambda x: str(x).lower() in row['series_description'].lower())
        pid_accn_filt = np.logical_and(df_other['patient_name'] == pid, df_other['accession_number'] == accn)
        view_pos_filt = np.logical_and(df_other['image_laterality'].str.lower() == row['image_laterality'].lower()
                                       , df_other['view_position'].str.lower() == row['view_position'].lower())
        df_ref = df_other[np.logical_and(np.logical_and(series_filt, pid_accn_filt), view_pos_filt)]

        ref_sop = df_ref['sopuid'].to_list()
        return ref_sop

    def get_first_GT_frame(self, sop):
        first_GT_frame = sorted(self.df[self.df['sopuid'] == sop]['frame_number'].to_list())[0]
        return first_GT_frame

    def get_bbox(self, sop, sop_colname='sopuid', offset=0, frame_number=None, cvat_frame=None):

        if frame_number is None and cvat_frame is None:
            df_sop = self.df[self.df[sop_colname] == sop]
            df_bbox = df_sop.apply(lambda x: (x['frame_number'], [x['xmin']+offset, x['ymin'], x['xmax']+offset, x['ymax']]), axis=1)
        elif frame_number is not None and cvat_frame is None:
            df_sop = self.df[np.logical_and(self.df[sop_colname] == sop, self.df['frame_number'] == int(frame_number))]
            df_bbox = df_sop.apply(lambda x: (int(frame_number), [x['xmin']+offset, x['ymin'], x['xmax']+offset, x['ymax']]), axis=1)
        elif cvat_frame is not None:
            df_sop = self.df[self.df[sop_colname] == sop]
            df_bbox = df_sop.apply(lambda x: (int(cvat_frame), [x['xmin'] + offset, x['ymin'], x['xmax'] + offset, x['ymax']]),axis=1)

        if df_bbox.empty:
            return []
        return list(df_bbox)