import cv2
import os
import glob
import logging
from Utilities.dataframe_util import DataframeUtil


def _hconcat_resize_image(im_orig, im_ref, new_h, interpolation=cv2.INTER_CUBIC):
    if new_h == None:
        new_h = im_orig.shape[0]

    im_orig_resize = cv2.resize(im_orig, (int(im_orig.shape[1] * new_h / im_orig.shape[0]), new_h), interpolation=interpolation)
    im_ref_resize = cv2.resize(im_ref, (int(im_ref.shape[1] * new_h / im_ref.shape[0]), new_h),
                               interpolation=interpolation)

    return cv2.hconcat([im_orig_resize, im_ref_resize])


def get_tomo_image_height(file_path):
    tomo_img_path = [f for f in glob.glob(os.path.join(file_path, '*.png')) if os.path.isfile(f)][0]
    im1 = cv2.imread(tomo_img_path)
    return im1.shape[0]

def copy_resize(img_fullpath, output_path, new_h, interpolation=cv2.INTER_CUBIC):
    im1 = cv2.imread(img_fullpath)
    try:
        im_resize = cv2.resize(im1, (int(im1.shape[1] * new_h / im1.shape[0]), new_h),
                                    interpolation=interpolation)
        fname = "z_" + os.path.split(img_fullpath)[1]
        cv2.imwrite(os.path.join(output_path, fname), im_resize)
    except Exception as e:
        logging.error("Error resizing {}: {}".format(img_fullpath, e))



def flip_img(im):
    img_flip = im[::-1, ::-1] #cv2.flip(img_fpath, 1)
    #cv2.imshow('output', img_flip_lr)
    #cv2.waitKey()
    return img_flip


def concat_img_to_reference(fullpath1, fullpath2, out_fullpath, new_h=None, flip_ref=False, df=None):
    im1 = cv2.imread(fullpath1)
    im2 = cv2.imread(fullpath2)


    im2 = add_bbox(im2, fullpath2, df)
    if new_h is not None:
        im1 = add_bbox(im1, fullpath1, df)

    if flip_ref:
        im2 = flip_img(im2)

    im_h = _hconcat_resize_image(im1, im2, new_h)
    cv2.imwrite(out_fullpath, im_h)


def add_bbox(im, filepath, df):
    ref_img = os.path.split(filepath)[1].replace('.png', '')
    if len(ref_img.split('-')) > 1:
        ref_frame = ref_img.split('-')[1]
        sop = ref_img.split('-')[0]
        bbox = df.get_bbox(sop, frame_number=ref_frame)
    else:
        sop = ref_img
        bbox = df.get_bbox(sop)

    color = (0, 0, 255)
    for b in bbox:
        x_min = int(b[1][0])
        y_min = int(b[1][1])
        x_max = int(b[1][2])
        y_max = int(b[1][3])

        cv2.line(im, (x_min, y_min),
                 (x_max, y_min), color, 3)
        cv2.line(im, (x_max, y_min),
                 (x_max, y_max), color, 3)
        cv2.line(im, (x_max, y_max),
                 (x_min, y_max), color, 3)
        cv2.line(im, (x_min, y_max),
                 (x_min, y_min), color, 3)
    return im


def concat_movie_to_reference(movie_path, ref_fullpath, outdir, flip_ref=False, df=None):
    tomo_img = sorted([f for f in glob.glob(os.path.join(movie_path, '*.png')) if os.path.isfile(f)])
    for img_fullpath in tomo_img:
        tomo_fname = os.path.split(img_fullpath)[1].replace('.png', '')
        ref_fname = os.path.split(ref_fullpath)[1].replace('.png', '')
        new_filename = tomo_fname + '_' + ref_fname + '.png'

        out_fullpath = os.path.join(outdir, new_filename)
        concat_img_to_reference(img_fullpath, ref_fullpath, out_fullpath, None, flip_ref, df)

    return len(tomo_img)