from CvatUtil.CvatModel import Task, Label, Attributes, AttributeInputType


def populate_default_task(name_prefix="", pid=None, accn=None, sop=None, label=None):
    task = Task()
    task.name = name_prefix
    if pid is not None and accn is not None and sop is not None:
        task.name = "[{}] PID: {} ACCN: {} SOP: {}".format(name_prefix, pid, accn, sop)
    task.owner = 1  # admin
    task.assignee = 4 # None to make task visible to all
    # bug_tracker is being used for pathology/biopsy findings and general text about study
    task.bug_tracker = "https://curemetrix-public.s3-us-west-2.amazonaws.com/PROD-InstructionsforReaderStudy-110920-0733.pdf"

    if label is None:
        label1 = _populate_default_label()
    else:
        label1 = _populate_label(label)
    task.labels = [label1]

    return task


def _populate_label(name):
    label2 = Label()
    if name == 'Density':
        label2.name = "Check if True"

        attr_chk1 = Attributes()
        attr_chk1.name = "a.) Is the mask (highlighted region in blue) covering the correct area?"
        attr_chk1.input_type = AttributeInputType.INPUT_TYPE_CHECKBOX
        attr_chk1.default_value = "false"
        attr_chk1.values = ["false"]

        attr_comment1 = Attributes()
        attr_comment1.name = "a.) Comment"
        attr_comment1.input_type = AttributeInputType.INPUT_TYPE_TEXT
        attr_comment1.default_value = ""

        attr_chk2 = Attributes()
        attr_chk2.name = "b.) Is the density class correct according to the BI-RADS 4th edition?"
        attr_chk2.input_type = AttributeInputType.INPUT_TYPE_CHECKBOX
        attr_chk2.default_value = "false"
        attr_chk2.values = ["false"]

        attr_comment2 = Attributes()
        attr_comment2.name = "b.) Comment"
        attr_comment2.input_type = AttributeInputType.INPUT_TYPE_TEXT
        attr_comment2.default_value = ""

        attr_chk3 = Attributes()
        attr_chk3.name = "c.) Is the density class correct according to the BI-RADS 5th edition?"
        attr_chk3.input_type = AttributeInputType.INPUT_TYPE_CHECKBOX
        attr_chk3.default_value = "false"
        attr_chk3.values = ["false"]

        attr_comment3 = Attributes()
        attr_comment3.name = "c.) Comment"
        attr_comment3.input_type = AttributeInputType.INPUT_TYPE_TEXT
        attr_comment3.default_value = ""

        label2.attributes = [attr_comment1, attr_chk1, attr_comment2, attr_chk2, attr_comment3, attr_chk3]

    return label2

def _populate_default_label():
    label_bbox = Label()
    label_bbox.name = "bbox"
    attr_comment = Attributes()
    attr_comment.name = "Comment"
    attr_comment.input_type = AttributeInputType.INPUT_TYPE_TEXT
    attr_comment.default_value = ""

    attr_keep = Attributes()
    attr_keep.name = "Keep or Ignore"
    attr_keep.input_type = AttributeInputType.INPUT_TYPE_RADIO
    attr_keep.default_value = "Keep"
    attr_keep.values = ["Keep", "Ignore"]

    attr_chk1 = Attributes()
    attr_chk1.name = "Suspicious ROI not originally marked"
    attr_chk1.input_type = AttributeInputType.INPUT_TYPE_CHECKBOX
    attr_chk1.default_value = "false"
    attr_chk1.values = ["false"]

    attr_chk2 = Attributes()
    attr_chk2.name = "Adding a smaller GT within the larger GT"
    attr_chk2.input_type = AttributeInputType.INPUT_TYPE_CHECKBOX
    attr_chk2.default_value = "false"
    attr_chk2.values = ["false"]
    label_bbox.attributes = [attr_comment, attr_keep, attr_chk1, attr_chk2]
    return label_bbox