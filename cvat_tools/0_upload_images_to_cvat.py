import argparse
import os
import time
from CvatUtil.CvatApi import CvatApi
from Utilities.dataframe_util import DataframeUtil
import multiprocessing
from functools import partial
import logging
import math

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
       # logging.FileHandler("debug.log"),
        logging.StreamHandler()
    ]
)
NUM_PROCESSES = multiprocessing.cpu_count()


def upload_data_parallel(img_path, df, chunk_size=250):
    unique_files = sorted(list(df.df['filename'].drop_duplicates()))

    #chunk_size = int(len(unique_files) / NUM_PROCESSES)
    if chunk_size == 0:
        chunk_size = 1

    max_chunk = math.ceil(len(unique_files) / chunk_size)
    chunks = [(i[0], max_chunk, unique_files[i[1]:i[1] + chunk_size]) for i in
              zip(range(0, max_chunk, 1), range(0, len(unique_files), chunk_size))]

    pool = multiprocessing.Pool(processes=NUM_PROCESSES)
    func = partial(create_cvat_task, img_path, df)
    pool.starmap(func, chunks)
    pool.close()
    pool.join()


def upload_data(img_path, df, chunk_size=250):
    unique_files = sorted(list(df.df['filename'].drop_duplicates()))

    max_chunk = math.ceil(len(unique_files) / chunk_size)
    chunks = [unique_files[i:i + chunk_size] for i
              in range(0, len(unique_files), chunk_size)]

    chunk_part = 1
    for chunk in chunks:
        create_cvat_task(img_path, df, chunk_part, max_chunk, chunk)
        chunk_part += 1


def create_cvat_task(basedir, df, chunk_part, tot_part, list_chunk):
    t0 = time.time()
    files_list = []
    for file in list_chunk:
        sopuid = file
        img_path = os.path.join(basedir, sopuid)
        if os.path.exists(img_path):
            files_list.append(img_path)
        logging.info(sopuid)

    # create task, get task_id
    new_api = CvatApi("cmangio - Part " + str(chunk_part+1) + "/" + str(tot_part))
    task_id = new_api.create_task()

    # create job in task
    frame_cnt = new_api.add_image_list_to_task(task_id, files_list)
    logging.info("# images to upload ({})".format(frame_cnt))
    t_0 = time.time()
    job_created = False
    while not job_created:
        time.sleep(1)
        job_created, job_id = new_api.get_last_job_id(task_id, 1)
    t_1 = time.time()
    logging.info("Upload time for Task# {} Job# {} Image Count ({}): {:0.2f}s"
              .format(task_id, job_id, frame_cnt, t_1 - t_0))

    # add annotation for image frames
    frame = 0
    bbox = []
    for file in list_chunk:
        df_bbox = df.get_bbox(file, sop_colname='filename', cvat_frame=frame)
        bbox += df_bbox
        frame += 1

    if len(bbox) > 0:
        new_api.add_annotations(task_id, job_id, bbox)

    t1 = time.time()
    logging.info("Total time for Task# {}: {:0.2f}s".format(task_id, job_id, t1 - t0))


def main():
    parser = argparse.ArgumentParser(description="Uploads view")
    parser.add_argument('-i', '--indir', type=str, help="input dir for pngs", required=True)
    parser.add_argument('-f', '--file', type=str, help="csv file with list of sop and GT", required=True)
    args = parser.parse_args()

    df_util = DataframeUtil(args.indir, args.file)
    img_path = os.path.join(args.indir)

    t_0 = time.time()
    upload_data(img_path, df_util)
    logging.info("Total time: {:0.2f}s".format(time.time() - t_0))


if __name__ == '__main__':
    main()
