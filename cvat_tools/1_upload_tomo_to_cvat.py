import argparse
import os
import glob
import time
import cv2
from CvatUtil.CvatApi import CvatApi
from Utilities.dataframe_util import DataframeUtil
import logging
import multiprocessing
from functools import partial

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[
       # logging.FileHandler("debug.log"),
        logging.StreamHandler()
    ]
)
NUM_PROCESSES = multiprocessing.cpu_count()
OTHER_SUBDIR = 'other'


def upload_data_parallel(img_path, df):
    df_tomo = df.get_tomo()

    chunk_size = int(df_tomo.shape[0] / NUM_PROCESSES)
    print(df_tomo.shape[0], chunk_size)
    if chunk_size == 0:
        chunk_size = 1
    chunks = [df_tomo.iloc[i:i + chunk_size, :] for i in range(0, df_tomo.shape[0], chunk_size)]

    pool = multiprocessing.Pool(processes=NUM_PROCESSES)
    func = partial(create_cvat_task, img_path, df)
    pool.map(func, chunks)
    pool.close()
    pool.join()


def upload_data(img_path, df):
    df_tomo = df.get_tomo()
    create_cvat_task(img_path, df, df_tomo)


def create_cvat_task(basedir, df, df_chunk):
    # loop though each tomo sop
    for i, row in df_chunk.iterrows():
        t0 = time.time()
        job_cnt = 0
        sopuid = row['sopuid']
        img_path = os.path.join(basedir, sopuid)
        if not os.path.isdir(img_path):
            continue
        if len(os.listdir(img_path)) == 0:
            continue
        pid = row['patient_name']
        accn = row['accession_number']
        logging.info(str(pid) + " " + str(accn) + " " + sopuid)

        px_notes = []
        # create task, get task_id
        new_api = CvatApi()
        new_api.set_task_name("Tomo GT Check", pid.replace('^', ''), accn, sopuid)
        task_exists = new_api.does_task_exists(new_api.task.name)
        if task_exists:
            continue

        task_id = new_api.create_task()

        # create job in task
        job_cnt += 1
        frame_cnt = new_api.add_data_to_task(task_id, img_path, os.path.join(img_path, 'other'))
        logging.info("# frames to upload ({})".format(frame_cnt))
        t_0 = time.time()
        job_created = False
        while not job_created:
            time.sleep(1)
            job_created, job_id = new_api.get_last_job_id(task_id, job_cnt)
        t_1 = time.time()
        logging.info("Upload time for Task# {} Job# {} Frame Count ({}): {:0.2f}s"
              .format(task_id, job_id, frame_cnt, t_1 - t_0))

        series_info = df.get_series_info(pid, accn)
        px_notes.append("Job #" + str(job_id) + " " + series_info + " Frames: 0-" + str(frame_cnt-1))

        # add annotation for tomo image frames
        df_bbox = df.get_bbox(sopuid)
        bbox = df_bbox
        for b in bbox:
            px_notes.append("Tomo GT Frame " + str(b[0]) + " bbox " + str(b[1]))

        if len(bbox) > 0:
            new_api.add_annotations(task_id, job_id, bbox)

        t1 = time.time()
        logging.info("Total time for Task# {}: {:0.2f}s".format(task_id, job_id, t1 - t0))

        px_notes_str = ' ////// '.join(p for p in px_notes)

        # update task_id with notes for "bug_tracker" field
        new_api.update_task_notes(task_id, px_notes_str)


def main():
    parser = argparse.ArgumentParser(description="Uploads tomo movie + 2D view into CVAT")
    parser.add_argument('-b', '--basedir', type=str, help="basedir for everything", required=True)
    parser.add_argument('-i', '--img_dir', type=str, help="subdir for tomo to upload w/ 'other' folder", required=True,
                        default='for_cvat')
    parser.add_argument('-f', '--file', type=str, help="csv with list of sop and GT", required=True)
    args = parser.parse_args()

    df_util = DataframeUtil(args.basedir, args.file)
    img_path = os.path.join(args.basedir, args.img_dir)
    t_0 = time.time()
    upload_data(img_path, df_util)
    logging.info("Total time: {:0.2f}s".format(time.time() - t_0))


if __name__ == '__main__':
    main()
